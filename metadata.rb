name              'itculate'
maintainer        'Ophir Horn'
maintainer_email  'opensource@itculate.io'
license           'Apache 2.0'
description       'Installs/Configures the ITculate agent'
long_description  IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version           '0.1.2'
source_url        'https://bitbucket.org/itculate/itculate-chef' if respond_to? :source_url

depends           'poise-python'
depends           'tar'

recipe 'itculate::default', 'Installs the ITculate agent'

%w{ debian ubuntu }.each do |os|
  supports os
end
